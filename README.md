## IO-291 - QA Baseline Database to be moved to Bitbucket with instructions on how to install:

## Prerequisites:
1. Prepared QABaseline demo data files.
2. Kettle setup is required.

## Steps to setup QABaseline database using QABaseline demo data files:
**1. Create QABaseline tenant:**

	a. Login to Smart application using super admin user.
	b. Navigate to Tenant Management page.
	c. Click on Add Tenant and Enter all details to mentioned there.
	d. Verify all #c details and create tenant.
	e. Run db deployment script for #d created tenant.
	f. Note down created TenantId for this QABaseline tenant.
	
**2. Add files into tenant directory in kettle installation directory.**

	a. Get Latest of 'ImplementationsRepo' from bitbucket workspace.
	b. Open Kettle installation directory and Go to 'TenantFiles' folder.
	c. Create folder with name of TenantId(#1.f) for tenant.
	d. Copy all QABaseline Demo data files from #a repository to #c tenant folder.
	e. Add suffix with current date to all #c QABaseline Demo data files.
	
**3. Steps to import data using #2 tenant files:**

	a. Open Spoon.bat from Kettle installation directory.
	b. Click on 'Explore Repository' icon from toolbar of the main screen.
	c. Browse and double click on 'QABaseline' folder from #b Repository explorer.
	d. Open master_job and double click on that.
	e. Navigate to 'Parameters' tab and add required parameters.
	f. Change parameters values as per your requirements(for e.g.):
		i. Postgres connection details: Environment specific details.
		ii. FILENAME_SUFIX: <should be same as #2.e>
		iii. TenantId: <should be same as #1.f>
		iv. UserId: <should be created from #1.f>
		v. Add values also for other parameters as per your requirements.
	g. Verify all #f parameters and click on Ok and save the master_job.
	h. Click on the 'Run' button to run the job.
	
**4. Verify all imported data in the QABaseline Database.**